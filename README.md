# Image Gallery Project

![Version](https://img.shields.io/badge/version-0.1-red.svg?cacheSeconds=2592000)
![Python Version](https://img.shields.io/badge/python-3.6-yellow)
![Elm Version](https://img.shields.io/badge/elm-0.19.1-blue)
[![GitHub license](https://img.shields.io/badge/license-MIT-green)](https://github.com/wyllisMonteiro/image_gallery/blob/master/LICENSE)

### Requirements

* Docker
    - [Windows](https://docs.docker.com/docker-for-windows/install/)
    - [Linux](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
    - [MacOS](https://docs.docker.com/docker-for-mac/install/)
* [docker-compose](https://docs.docker.com/compose/install/)

### Build and install 

Go to the project folder and start the containers

```sh
$ git clone https://gitlab.com/wyllisMonteiro/image-gallery.git
$ cd image_gallery/
$ docker-compose up --build
```

### Documentations
- BACK : in progress
- FRONT : in progress
- DOCKER : in progress

### Contributing
Please read and follow [CONTRIBUTING.md](https://gitlab.com/wyllisMonteiro/image-gallery/-/blob/master/CONTRIBUTING.md)

### Authors
Monteiro Wyllis : wyllismonteiro@gmail.com

### License
Please read and follow [LICENSE](https://gitlab.com/wyllisMonteiro/image-gallery/-/blob/master/LICENSE)